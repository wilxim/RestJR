package com.aes.ws.rest.restService;



import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aes.ws.rest.rest.vo.VOUsuario;

@Path("/aes")
public class ServiceLoginJR {
	
	@POST
	@Path("/validaUsuario")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public VOUsuario validaUsuario(VOUsuario voU) {
		voU.setUserValido(false);
		if(voU.getUsuario().equals("jr") &&  voU.getPassword().equals("revolutiones")){
			voU.setUserValido(true);
		}
			
			return voU;
		
	}

}
